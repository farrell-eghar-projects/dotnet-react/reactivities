import React from 'react';
import { observer } from 'mobx-react-lite';
import * as Yup from "yup";
import { Form, Formik } from 'formik';
import { Button } from 'semantic-ui-react';
import MyTextInput from '../../app/common/form/MyTextInput';
import MyTextArea from '../../app/common/form/MyTextArea';
import { useStore } from '../../app/stores/store';

interface Props {
	setEditMode: (editMode: boolean) => void;
}

export default observer(function ProfileEditForm({setEditMode}: Props) {
	const {profileStore: {profile, updateProfile}} = useStore();
	
	const validationSchema = Yup.object({
		displayName: Yup.string().required()
	});
	
	return (
		<Formik
			initialValues={{displayName: profile?.displayName, bio: profile?.bio}}
			onSubmit={(values) => {
				updateProfile(values).then(() => {
					setEditMode(false);
				})
			}}
			validationSchema={validationSchema}
		>
			{({isSubmitting, isValid, dirty}) => (
				<Form className="ui form" autoComplete="off">
					<MyTextInput placeholder="Display Name" name="displayName" />
					<MyTextArea rows={3} placeholder="Add your bio" name="bio" />
					<Button
						positive
						type="submit"
						loading={isSubmitting}
						disabled={!isValid || !dirty || isSubmitting}
						floated='right'
						content="Update Profile"
						fluid
					/>
				</Form>
			)}
		</Formik>
	)
})